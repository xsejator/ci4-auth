<?php

namespace Sejator\Auth\Controllers;

use CodeIgniter/Controller;

class AuthController extends Controller
{
    public function index()
    {
        return view('welcome_message');
    }
}
